
# if no "v" var given, default to package version
v ?= $(shell node -pe "require('./package.json').version")

# expand variable (so we can use it on branches w/o package.json)
VERSION := $(v)

# get x.x.* part of the version number
MINOR_VERSION = `echo $(VERSION) | sed 's/\.[^.]*$$//'`

# Command line paths
KARMA = ./node_modules/karma/bin/karma
ESLINT = ./node_modules/eslint/bin/eslint.js
MOCHA = ./node_modules/mocha/bin/_mocha
SMASH = ./node_modules/.bin/smash
ROLLUP = ./node_modules/.bin/rollup
UGLIFY = ./node_modules/uglify-js/bin/uglifyjs
COVERALLS = ./node_modules/coveralls/bin/coveralls.js
RIOT_CLI = ./node_modules/.bin/halvalla-riot
CHOKIDAR = ./node_modules/.bin/chokidar

# folders
DIST = dist/halvalla-riot/
LIB = lib/
CONFIG = config/

GENERATED_FILES = halvalla-riot.js halvalla-riot+compiler.js


test: eslint test-mocha test-karma

eslint: raw
	# check code style
	@ $(ESLINT) -c ./.eslintrc.json lib test

test-mocha: raw
	RIOT=../../dist/halvalla-riot/halvalla-riot.js $(MOCHA) -- test/specs/server

tags: raw
	@ $(RIOT_CLI) --silent test/tag dist/tags.js

test-karma: raw
  # Test halvalla-riot+compiler.js
	#@ TEST_FOLDER=browser/compiler $(KARMA) start test/karma.conf.js
	# Test only halvalla-riot.js and generate the coverage
	#@ TEST_FOLDER=browser/halvalla-riot $(KARMA) start test/karma.conf.js

test-coveralls: raw
	@ RIOT_COV=1 cat ./coverage/report-lcov/lcov.info | $(COVERALLS)

test-sauce: raw
	# run the halvalla-riot tests on saucelabs
 	#	@ SAUCELABS=1 make test-karma

test-chrome: raw
 	#	@ DEBUG=1 TEST_FOLDER=browser/halvalla-riot ${KARMA} start test/karma.conf.js --browsers=Chrome --no-single-run --watch

compare: raw
	# compare the current release with the previous one
	du -h halvalla-riot.min.js halvalla-riot+compiler.min.js
	du -h dist/halvalla-riot/halvalla-riot.min.js dist/halvalla-riot/halvalla-riot+compiler.min.js

raw:
	# build halvalla-riot
	@ mkdir -p $(DIST)
	# Default builds UMD
	@ $(ROLLUP) lib/halvalla-riot.js --config $(CONFIG)rollup.config.js > $(DIST)halvalla-riot.js
	@ $(ROLLUP) lib/halvalla-riot+compiler.js --config $(CONFIG)rollup.config.js > $(DIST)halvalla-riot+compiler.js
	# Chrome Security Policy build
	@ $(ROLLUP) lib/halvalla-riot.js --config $(CONFIG)rollup.config.csp.js > $(DIST)halvalla-riot.csp.js

clean:
	# clean $(DIST)
	@ rm -rf $(DIST)

halvalla-riot: clean raw test

min: halvalla-riot
	# minify halvalla-riot
	@ for f in $(GENERATED_FILES); do \
		$(UGLIFY) $(DIST)$$f \
			--comments \
			--toplevel \
			--mangle \
			--compress  \
			-o $(DIST)$${f%.*}.min.js; \
		done

perf: halvalla-riot
	# run the performance tests
	@ node test/performance/benchmarks ../halvalla-riot.2.6.1 --expose-gc
	@ node test/performance/benchmarks ../../../halvalla-riot --expose-gc
	@ node test/performance/benchmarks ../../../dist/halvalla-riot/halvalla-riot --expose-gc

perf-leaks: halvalla-riot
	# detect memory leaks
	@ node --expose-gc test/performance/memory

watch:
	# watch and rebuild halvalla-riot and its testswatch:
	@ $(CHOKIDAR) lib -c 'make raw & make tags'

build: min
	# generate halvalla-riot.js & halvalla-riot.min.js
	@ cp dist/halvalla-riot/* .
	# write version in halvalla-riot.js
	@ sed -i'' 's/WIP/v$(VERSION)/g' halvalla-riot*.js


bump:
	# grab all latest changes to master
	# (if there's any uncommited changes, it will stop here)
	# bump version in *.json files
	@ mv package-lock.json package-lock.tmp
	@ sed -i'' 's/\("version": "\)[^"]*/\1'$(VERSION)'/' *.json
	@ mv package-lock.tmp package-lock.json
	@ make build
	@ git status --short

bump-undo:
	# remove all uncommited changes
	@ git reset --hard


version:
	# @ git checkout master
	# create version commit
	@ git status --short
	@ git add --all
	@ git commit -am "$(VERSION)"
	@ git log --oneline -2
	# create version tag
	@ git tag -a 'v'$(VERSION) -m $(VERSION)
	@ git describe

version-undo:
	# remove the version tag
	@ git tag -d 'v'$(VERSION)
	@ git describe
	# remove the version commit
	@ git reset `git rev-parse :/$(VERSION)`
	@ git reset HEAD^
	@ git log --oneline -2


release: bump version

release-undo:
	make version-undo
	make bump-undo


publish:
	# push new version to npm and github
	# (github tag will also trigger an update in bower, component, cdnjs, etc)
	@ npm publish
	@ git push origin master
	@ git push origin master --tags

.PHONY: test min eslint test-mocha test-compiler test-coveralls test-sauce compare raw halvalla-riot perf watch tags perf-leaks build bump bump-undo version version-undo release-undo publish
